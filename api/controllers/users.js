const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const mongoose = require('mongoose')
const User = require('../models/user')

module.exports.signUp = (req, res, next) => {
    User.find({email: req.body.email})
        .then(user => {
            if(user.length > 1) {
                res.status(409).json({
                    msg: "Mail exists"
                })
            }
            else {
                bcrypt.hash(req.body.password, 10, (err, hash) => {
                    if(err){
                        return res.status(500).json({
                            error: err
                        })
                    } else {
                        const user = new User({
                            _id: new mongoose.Types.ObjectId(),
                            email: req.body.email,
                            password: hash
                        })
                        user.save()
                           .then(result => {
                               res.status(201).json({
                                   msg: 'User created'
                               })
                           })
                           .catch(err => {
                               res.status(500).json({
                                   error: err
                               })
                           })
                    } 
                })
            }
        })
}


module.exports.login = (req, res, next) => {
    User.find({
            email: req.body.email
        })
        .then(user => {
            if (user.legth < 1) {
                res.status(401).json({
                    msg: 'Auth fail'
                })
            }
            bcrypt.compare(req.body.password, user[0].password, (err, result) => {
                if (err) {
                    return res.status(401).json({
                        msg: 'Auth failed'
                    })
                }
                if (result) {
                    const token = jwt.sign({
                            email: user[0].email,
                            userId: user[0]._id
                        },
                        process.env.JWT_KEY, {
                            expiresIn: "1h"
                        })
                    return res.status(200).json({
                        msg: 'Auth successful',
                        token: token
                    })
                }
                res.status(401).json({
                    msg: 'Auth failed'
                })
            })
        })
        .catch(err => {
            res.status(500).json({
                error: err,
                msg: 'mostra aqui'
            })
        })

}


module.exports.delOneUser = (req, res, next) => {
    User.deleteOne({
            _id: req.params.userId
        })
        .then(rs => {
            res.status(200).json({
                msg: 'User deleted'
            })
        })
        .catch(err => {
            res.status(500).json({
                error: err
            })
        })
}
