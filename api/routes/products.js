const express = require('express')
const router = express.Router()

const productsCtrl = require('../controllers/products')

const multer =  require('multer')

const checkAuth = require('../middleware/check-auth')

const storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, './uploads/')
    },
    filename: function(req, file, cb) {
        cb(null, new Date().toISOString()+file.originalname )
    }
})

const fileFilter = (req, file, cb) => {
    if(file.mimetype === 'image/jpeg' || filemime === 'image/png') {
        cb(null, true)
    }else {
        cb(null, false)
    }
}

const upload = multer({storage: storage,
    limits: {
        fieldSize: 1024 * 1024 * 5
    },
    fileFilter: fileFilter
}) 


router.post('/', checkAuth, upload.single('productImage'), productsCtrl.createProduct) 

router.get('/', checkAuth, productsCtrl.getAllProducts)

router.get('/:productId', checkAuth, productsCtrl.getOneProduct)

router.patch('/:productId', checkAuth, productsCtrl.updateOneProduct)

router.delete('/:id', checkAuth, productsCtrl.delOneProduct)




module.exports = router