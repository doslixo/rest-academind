const express = require('express')
const router = express.Router()

const checkAuth = require('../middleware/check-auth')
const ordersCtrl = require('../controllers/orders')


router.post('/', checkAuth, ordersCtrl.createOrder)

router.get('/', checkAuth, ordersCtrl.getAllOrders)

router.get('/:orderId', checkAuth, ordersCtrl.getOneOrder)

router.delete('/:orderId', checkAuth, ordersCtrl.delOneOrder)


module.exports = router